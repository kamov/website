---
title: "Introduction to Hanafuda Koikoi"
date: 2021-09-09T22:30:02+02:00
tags: [japan, hanafuda]
---

Hanafuda is a popular card game originating from Japan that features a set of 48 cards adorned with beautiful drawings of plants and seasons, hence the name "Hanafuda" — 花札, which means flower cards in Japanese. The game is known as Hanafuda Koikoi, and its objective is to collect various combinations of cards called Yaku to accumulate more points than your opponent. Each set of cards comprises 12 different plants and seasons, with four cards for each. Along with the plant/season groupings, the Yaku combinations offer players an opportunity to gain more points within the game. The goal of the game is to collect more points than your opponent.

## Cards

### January

Pine - Matsu 松

:icon[/static/svg/hanafuda/matsu1.svg]
:icon[/static/svg/hanafuda/matsu2.svg]
:icon[/static/svg/hanafuda/matsu3.svg]
:icon[/static/svg/hanafuda/matsu4.svg]

### February

Plum - Ume 梅

:icon[/static/svg/hanafuda/ume1.svg]
:icon[/static/svg/hanafuda/ume2.svg]
:icon[/static/svg/hanafuda/ume3.svg]
:icon[/static/svg/hanafuda/ume4.svg]

### March

Cherry blossom - Sakura 桜

:icon[/static/svg/hanafuda/sakura1.svg]
:icon[/static/svg/hanafuda/sakura2.svg]
:icon[/static/svg/hanafuda/sakura3.svg]
:icon[/static/svg/hanafuda/sakura4.svg]

### April

Wisteria - Fuji 藤

:icon[/static/svg/hanafuda/fuji1.svg]
:icon[/static/svg/hanafuda/fuji2.svg]
:icon[/static/svg/hanafuda/fuji3.svg]
:icon[/static/svg/hanafuda/fuji4.svg]

### May

Iris - Shobu 菖蒲

:icon[/static/svg/hanafuda/ayame1.svg]
:icon[/static/svg/hanafuda/ayame2.svg]
:icon[/static/svg/hanafuda/ayame3.svg]
:icon[/static/svg/hanafuda/ayame4.svg]

### June

Peony - Botan 牡丹

:icon[/static/svg/hanafuda/botan1.svg]
:icon[/static/svg/hanafuda/botan2.svg]
:icon[/static/svg/hanafuda/botan3.svg]
:icon[/static/svg/hanafuda/botan4.svg]

### July

Clover - Hagi 萩

:icon[/static/svg/hanafuda/hagi1.svg]
:icon[/static/svg/hanafuda/hagi2.svg]
:icon[/static/svg/hanafuda/hagi3.svg]
:icon[/static/svg/hanafuda/hagi4.svg]

### August

Susuki - Susuki 芒

:icon[/static/svg/hanafuda/susuki1.svg]
:icon[/static/svg/hanafuda/susuki2.svg]
:icon[/static/svg/hanafuda/susuki3.svg]
:icon[/static/svg/hanafuda/susuki4.svg]

### September

Chrysanthemum - Kiku 菊

:icon[/static/svg/hanafuda/kiku1.svg]
:icon[/static/svg/hanafuda/kiku2.svg]
:icon[/static/svg/hanafuda/kiku3.svg]
:icon[/static/svg/hanafuda/kiku4.svg]

### October

Maple - Momiji 紅葉

:icon[/static/svg/hanafuda/momiji1.svg]
:icon[/static/svg/hanafuda/momiji2.svg]
:icon[/static/svg/hanafuda/momiji3.svg]
:icon[/static/svg/hanafuda/momiji4.svg]

### November

Willow - Yanagi 柳

:icon[/static/svg/hanafuda/yanagi1.svg]
:icon[/static/svg/hanafuda/yanagi2.svg]
:icon[/static/svg/hanafuda/yanagi3.svg]
:icon[/static/svg/hanafuda/yanagi4.svg]

### December

Paulownia - Kiri 桐

:icon[/static/svg/hanafuda/kiri1.svg]
:icon[/static/svg/hanafuda/kiri2.svg]
:icon[/static/svg/hanafuda/kiri3.svg]
:icon[/static/svg/hanafuda/kiri4.svg]

## Gameplay

Hanafuda is a two-player game that starts with the selection of the Oya, the player who takes the first turn. Each player chooses one card, and the person who selects the card from the earlier month out of the two becomes the Oya.

After the Oya has been chosen, the game proceeds to the next phase. The cards are reshuffled, and eight cards are placed on the table. Each player receives a deck of eight cards.

The players take turns, beginning with the Oya, and each turn consists of two phases. In the first phase, the player attempts to match a card from their deck with any card present on the table. Matching is done by the month or plant on the card. If a match is made, the player takes both cards and keeps them separate from the deck of cards. If a match is not possible, the player has to select any card from their deck and place it on the table.

After the first phase is complete, the player selects a single card from the unused pile and tries to match it with any card left on the table. If a match is made, the player takes both cards. If there is no match possible, the player has to leave the card on the table.

Once both phases of the turn are complete, the turn comes to an end, and the next player takes their turn.

During the game, players have the opportunity to form a special group of cards called Yaku, which carries a certain number of points. Once a player has collected any single Yaku, they can decide whether to end the game and receive the points for the Yaku, or continue playing by calling "Koikoi" in the hope of collecting more Yaku and points.

The challenge arises once a player calls Koikoi, as the opponent can attempt to collect any Yaku as well. If the opponent succeeds in collecting a Yaku after a Koikoi has been called, they can choose to end the game or call Yaku, which would nullify the Yaku points of the player who called Koikoi. Only the opponent would receive their points for the Yaku in this case.

If a player successfully collects multiple Yaku, the points are added up, with one exception — the "lights" Yaku (Shikou, Gokou, Ameshikou, etc.). With these Yaku, it's only possible to upgrade to a higher-valued one.

Certain Yaku carry incremental points. For instance, the Kasu Yaku is worth one point for every ten cards. However, if a player manages to collect more cards, each additional card is valued at one point. For example, if a player has 12 Kasu cards, they will be awarded three points.

## Yaku

### Gokou 五光

Points: 10点

:icon[/static/svg/hanafuda/matsu4.svg]
:icon[/static/svg/hanafuda/sakura4.svg]
:icon[/static/svg/hanafuda/susuki4.svg]
:icon[/static/svg/hanafuda/yanagi4.svg]
:icon[/static/svg/hanafuda/kiri4.svg]

Five light cards, can't be combined with any other light combinations.

### Shikou 四光

Points: 8点

:icon[/static/svg/hanafuda/matsu4.svg]
:icon[/static/svg/hanafuda/sakura4.svg]
:icon[/static/svg/hanafuda/susuki4.svg]
:icon[/static/svg/hanafuda/kiri4.svg]

Four light cards excluding "Ono no Michikaze", can't be combined with any other light combinations.

### Ameshikou 雨四光

Points: 7点

:icon[/static/svg/hanafuda/yanagi4.svg]

\+ any 3 of

:icon[/static/svg/hanafuda/matsu4.svg]
:icon[/static/svg/hanafuda/sakura4.svg]
:icon[/static/svg/hanafuda/susuki4.svg]
:icon[/static/svg/hanafuda/kiri4.svg]

Four light cards including "Ono no Michikaze", can't be combined with any other light combinations.

### Sankou 三光

Points: 5点

Any 3 of

:icon[/static/svg/hanafuda/matsu4.svg]
:icon[/static/svg/hanafuda/sakura4.svg]
:icon[/static/svg/hanafuda/susuki4.svg]
:icon[/static/svg/hanafuda/kiri4.svg]

Three light cards excluding "Ono no Michikaze", can't be combined with any other light combinations.

### Inoshikachou 猪鹿蝶

Points: 5点＋α

:icon[/static/svg/hanafuda/hagi4.svg]
:icon[/static/svg/hanafuda/momiji4.svg]
:icon[/static/svg/hanafuda/botan4.svg]

Boar, deer, and butterfly.

### Tsukimizake 月見酒

Points: 5点

:icon[/static/svg/hanafuda/susuki4.svg]
:icon[/static/svg/hanafuda/kiku4.svg]

Moon and sake cup.

### Hanamizake 花見酒

Points: 5点

:icon[/static/svg/hanafuda/sakura4.svg]
:icon[/static/svg/hanafuda/kiku4.svg]

Cherry blossom curtain and sake cup.

### Tane 種・タネ

Points: 1点＋α

Any 5 of

:icon[/static/svg/hanafuda/ume4.svg]
:icon[/static/svg/hanafuda/susuki3.svg]
:icon[/static/svg/hanafuda/fuji4.svg]
:icon[/static/svg/hanafuda/kiku4.svg]
:icon[/static/svg/hanafuda/ayame4.svg]
:icon[/static/svg/hanafuda/momiji4.svg]
:icon[/static/svg/hanafuda/botan4.svg]
:icon[/static/svg/hanafuda/yanagi3.svg]
:icon[/static/svg/hanafuda/hagi4.svg]

Five Tane cards, one additional point per each additional Tane card.

### Akatan-Aotan 赤短・青短

Points: 10点＋α

:icon[/static/svg/hanafuda/matsu3.svg]
:icon[/static/svg/hanafuda/ume3.svg]
:icon[/static/svg/hanafuda/sakura3.svg]
:icon[/static/svg/hanafuda/botan3.svg]
:icon[/static/svg/hanafuda/kiku3.svg]
:icon[/static/svg/hanafuda/momiji3.svg]

Three red poetry ribbons and three blue ribbons.

### Akatan 赤短

Points: 5点＋α

:icon[/static/svg/hanafuda/matsu3.svg]
:icon[/static/svg/hanafuda/ume3.svg]
:icon[/static/svg/hanafuda/sakura3.svg]

Three red poetry ribbons.

### Aotan 青短

Points: 5点＋α

:icon[/static/svg/hanafuda/botan3.svg]
:icon[/static/svg/hanafuda/kiku3.svg]
:icon[/static/svg/hanafuda/momiji3.svg]

Three blue ribbons.

### Tan 短

Points: 1点＋α

Any 5 of

:icon[/static/svg/hanafuda/matsu3.svg]
:icon[/static/svg/hanafuda/ume3.svg]
:icon[/static/svg/hanafuda/sakura3.svg]
:icon[/static/svg/hanafuda/fuji3.svg]
:icon[/static/svg/hanafuda/ayame3.svg]
:icon[/static/svg/hanafuda/botan3.svg]
:icon[/static/svg/hanafuda/hagi3.svg]
:icon[/static/svg/hanafuda/kiku3.svg]
:icon[/static/svg/hanafuda/momiji3.svg]
:icon[/static/svg/hanafuda/yanagi2.svg]

Five ribbon cards, one additional point per each additional ribbon card.

### Kasu カス

Points: 1点＋α

Any 10 of

:icon[/static/svg/hanafuda/matsu1.svg]
:icon[/static/svg/hanafuda/matsu2.svg]
:icon[/static/svg/hanafuda/ume1.svg]
:icon[/static/svg/hanafuda/ume2.svg]
:icon[/static/svg/hanafuda/sakura1.svg]
:icon[/static/svg/hanafuda/sakura2.svg]
:icon[/static/svg/hanafuda/fuji1.svg]
:icon[/static/svg/hanafuda/fuji2.svg]
:icon[/static/svg/hanafuda/ayame1.svg]
:icon[/static/svg/hanafuda/ayame2.svg]
:icon[/static/svg/hanafuda/botan1.svg]
:icon[/static/svg/hanafuda/botan2.svg]
:icon[/static/svg/hanafuda/hagi1.svg]
:icon[/static/svg/hanafuda/hagi2.svg]
:icon[/static/svg/hanafuda/susuki1.svg]
:icon[/static/svg/hanafuda/susuki2.svg]
:icon[/static/svg/hanafuda/kiku1.svg]
:icon[/static/svg/hanafuda/kiku2.svg]
:icon[/static/svg/hanafuda/momiji1.svg]
:icon[/static/svg/hanafuda/momiji2.svg]
:icon[/static/svg/hanafuda/yanagi1.svg]
:icon[/static/svg/hanafuda/kiri1.svg]
:icon[/static/svg/hanafuda/kiri2.svg]
:icon[/static/svg/hanafuda/kiri3.svg]

Ten plain cards, one additional point per each additional plain card.

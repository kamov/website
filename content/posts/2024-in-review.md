---
title: 2024-in-review
date: 2025-01-01T22:09:23.988Z
---

Hey, let me start by saying thanks for reading! It's a pretty surprising thing
that there are in fact people who have read this website and found it
interesting. This website is pretty random and crude, I might make it even
better in the coming year.

Previous year was pretty busy, there was a lot that happened, and I haven't read
as much as I wanted. In the coming year I would like to read at least one book a
week, a challenge of sorts. I might make some kind of a table on this website to
motivate myself to read more.

In the past year I've improved my Rust and Haskell skills, I also wrote a
[static site generator][hauchiwa] from scratch for this website in Rust. My
goals for the next year is to make the generator more feature complete and
robust, as well as to try to learn more about OCaml, Scheme and Zig.

Some projects currently worth checking out in my opinion are:

- [Zed](https://zed.dev)
- [Servo](https://servo.org)
- [Typst](https://typst.app/)
- [Forgejo](https://forgejo.org/)
- [Melange](https://melange.re)
- [Lean](https://lean-lang.org/)
- [Fish](https://fishshell.com/)

[hauchiwa]: https://crates.io/crates/hauchiwa
